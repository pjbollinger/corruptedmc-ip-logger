package me.magicpat2010.CorruptedIPLogger;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerListener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class CorruptedIPLoggerPlayerListener extends PlayerListener{
	public CorruptedIPLogger plugin;

	//String pass = CorruptedIPLogger.getConfigInformation().getString("sqldatabase.password");
	//String url = "jdbc:mysql://" + CorruptedIPLogger.getConfigInformation().getString("sqldatabase.server") + ":3306/" + CorruptedIPLogger.getConfigInformation().getString("sqldatabase.database");
	//String user = configFile.getString("sqldatabase.user");
	public CorruptedIPLoggerPlayerListener(CorruptedIPLogger instance) {
		plugin = instance;
	}
	
	public void onPlayerJoin(PlayerJoinEvent event){
		Player player = event.getPlayer();
		String playerIP = player.getAddress().toString();
		int colonPos = playerIP.indexOf(":");
		playerIP = playerIP.substring(1, colonPos);
		//player.sendMessage("Your IP is: " + playerIP);
		String plugininfo = plugin.callConfig("sqldatabase.server");
		plugin.log.info(plugininfo);
	}
	
	public void onPlayerQuit(PlayerQuitEvent event){
		plugin = new CorruptedIPLogger();
		//plugin.log.info(plugin.getConfigInformation().getString("sqldatabase.server"));
	}
}
