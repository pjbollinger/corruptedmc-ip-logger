package me.magicpat2010.CorruptedIPLogger;

import java.util.logging.Logger;
import java.io.File;/*
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;*/

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.configuration.file.FileConfiguration;
//import org.bukkit.configuration.file.YamlConfiguration;

public class CorruptedIPLogger extends JavaPlugin {	
	protected FileConfiguration plugininfo;
	private final CorruptedIPLoggerPlayerListener playerListener = new CorruptedIPLoggerPlayerListener(this);
	Logger log = Logger.getLogger("Minecraft");
	
	//declare Files (.ymls)
	public File configFile;
	
	//declare FileCOnfigurations (each File has one FileConfiguration)
	protected FileConfiguration config;
	protected FileConfiguration config1;
	public void striptease(){
		
	}
	public void onEnable() {
		//Initialize .ymls Files and FileCOnfigurations
		classForConfiguration();
		//FileConfiguration configtest = getConfigInformation();
		//End Initializing .ymls
		plugininfo = getConfig();
		PluginManager pm = this.getServer().getPluginManager();
		pm.registerEvent(Event.Type.PLAYER_JOIN, playerListener, Event.Priority.Normal, this);
		log.info("Your plugin has been enabled.");
		log.info(callConfig("sqlconnection.user"));
	}
	
	public void onDisable() {
		//saveYamls();
		log.info("Your plugin has been disabled.");
	}
	/*
	private void firstRun() throws Exception {
		if(!configFile.exists()){
			configFile.getParentFile().mkdirs();
			copy(getResource("config.yml"), configFile);
		}
	}

	private void copy(InputStream in, File file) {
		try {
			OutputStream out = new FileOutputStream(file);
			byte [] buf = new byte[1024];
			int len;
			while((len=in.read(buf))>0){
				out.write(buf,0,len);
			}
			out.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadYamls() {
		try {
			config.load(configFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveYamls() {
		try {
			config.save(configFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}*/
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		Player player = null;
		if (sender instanceof Player) {
			player = (Player) sender;
		}
		
		if(cmd.getName().equalsIgnoreCase("ciplogger")){ //If the player has type /ciplogger then do the following
			sender.sendMessage("Thank you for using CorruptedIPLogger!");
			sender.sendMessage("Sincerely, magicpat2010");
			return true;
		} else if (cmd.getName().equalsIgnoreCase("ciploggerplayer")){
			if (player == null){
				sender.sendMessage("This command can only be run by a player");
			} else {
				sender.sendMessage("You are a player and used the command ciploggerplayer.");
			}
			return true;
		}
		return false;
	}/*
	public FileConfiguration getConfigInformation() {
		if (config != null) {
			return config;
		}
		if (configFile == null){
			configFile = new File(getDataFolder(), "config.yml"); //getDataFolder = plugins/<name>/
			
			try {
				firstRun();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		config = new YamlConfiguration();
		loadYamls();
		return config;
	}*/
	public void classForConfiguration(){
		try{
			config1 = getConfig();
			File conf = new File("plugins" + File.separator + "CorruptedIPLogger" + File.separator + "config.yml");
			conf.mkdir();
			if(!config1.contains("sqlconnection.server")){
				config1.set("sqlconnection.server", "127.0.0.1");
			}
			if(!config1.contains("sqlconnection.user")){
				config1.set("sqlconnection.user", "root");
			}
            if(!config1.contains("sqlconnection.password")){
            	config1.set("sqlconnection.password", "root");
            }
            if(!config1.contains("sqlconnection.database")){
            	config1.set("sqlconnection.database", "iplog");
            }
            saveConfig();
		}catch(Exception e1){
			e1.printStackTrace();
		}
	}
	public String callConfig(String path){
		String var = config1.getString(path);
		return var;
	}
}